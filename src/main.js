const http = require("http");
const fs = require("fs");
// const path = require('path');
const { v4: uuidv4 } = require("uuid");
const htmlF = "./dataFiles/index.html";
const jsonF = "./dataFiles/data.json";
const statusco = require("http").STATUS_CODES;

const server = http.createServer((request, response) => {
  if (request.method === "GET" && request.url === "/html") {
    fs.readFile(htmlF, (err, data) => {
      if (err) {
        response.writeHead(404);
        response.end("File not found");
      } else {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.end(data);
      }
    });
  } else if (request.method === "GET" && request.url === "/json") {
    fs.readFile(jsonF, (err, data) => {
      if (err) {
        response.writeHead(404);
        response.end("File not found");
      } else {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.end(data);
      }
    });
  } else if (request.method === "GET" && request.url === "/uuid") {
    const uuid = uuidv4();
    response.writeHead(200, { "Content-Type": "application/json" });
    response.end(JSON.stringify({ uuid }));
  } else if (request.method === "GET" && request.url.startsWith("/status/")) {
    const statusCode = parseInt(request.url.split("/")[2]);
    if (isNaN(statusCode) || statusCode < 100 || statusCode > 599) {
      response.writeHead(400);
      response.end("Invalid status code");
    } else {
      response.writeHead(statusCode, { "Content-Type": "text/plain" });
      response.end(statusco[statusCode]);
    }
  } else if (request.method === "GET" && request.url.startsWith("/delay/")) {
    const delaySec = parseInt(request.url.split("/")[2]);

    setTimeout(() => {
      response.writeHead(200);
      response.end("Status code dropped");
    }, delaySec * 1000);
  } else {
    response.writeHead(404);
    response.end("Page not found");
  }
});

server.listen(9000);
